THIS CODE IS WRITTEN BY HADI POURANSARI

CONTACT: HADIP [AT] STANFORD [D.O.T.] EDU

-----------------------------------------


Please read **refman.pdf** for a brief description of the methods and classes implemented in the code.

You need to install:

1. The [Eigen](http://eigen.tuxfamily.org/dox/GettingStarted.html) library.

2. The [SCOTCH](https://www.labri.fr/perso/pelegrin/scotch/) library.

Make sure to change the PATH to these libraries in the makefile.